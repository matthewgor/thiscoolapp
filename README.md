# ThisCoolApp

This is a test area for proof of concept, and a possible starting place for other work.  The container runs Alpine and run an infinite loop printing date and iteration number to stdout.

To push an image to this project use:

` docker push registry.gitlab.com/matthewgor/thiscoolapp:VERSION `

To pull the image for example use this:

` docker login registry.gitlab.com `

` docker pull registry.gitlab.com/matthewgor/thiscoolapp:latest `


